module.exports = (grunt) ->
  
  # Load grunt tasks automatically
  require("load-grunt-tasks")(grunt)
  
  grunt.initConfig
  
    pkg: grunt.file.readJSON('package.json')
    
    config:
      bowerPath: "bower_components"
      assetPath: "build/public/assets"
      sourcePath:
        css: "source/css"
        js: "source/js"
      buildPath:
        css: "<%= config.assetPath %>/css"
        js: "<%= config.assetPath %>/js"
    
    uglify:
      dist:
        files: "<%= uglify.files %>"
      dev:
        options:
          beautify: true
        files: "<%= uglify.files %>"
      files: [
        src: [
          "<%= config.bowerPath %>/jquery/jquery.js"
          "<%= config.bowerPath %>/jquery.easing/js/jquery.easing.js"
          "<%= config.bowerPath %>/jquery-validation/dist/jquery.validate.js"
          "<%= config.bowerPath %>/jquery-parallax/scripts/jquery.parallax-1.1.3.js"
          "<%= config.bowerPath %>/jquery-hoverintent/jquery.hoverIntent.js"
          "<%= config.bowerPath %>/jquery-cycle2/build/jquery.cycle2.js"
          "<%= config.sourcePath.js %>/plugins.js"
          "<%= config.sourcePath.js %>/main.js"
        ]
        dest: "<%= config.buildPath.js %>/main.js"
      ]
      
    sass:
      dist:
        options:
          outputStyle: "compressed"
        expand: true
        cwd: "<%= config.sourcePath.css %>"
        src: "**/*.scss"
        dest: "<%= config.buildPath.css %>"
        ext: ".css"
      dev:
        options:
          sourceMap: true
        expand: true
        cwd: "<%= config.sourcePath.css %>"
        src: "**/*.scss"
        dest: "<%= config.buildPath.css %>"
        ext: ".css"
    
    watch:
      options:
        livereload: true
        atBegin: true
      css:
        files: "<%= config.sourcePath.css %>/**/*.scss"
        tasks: ['sass:dev']
      js:
        files: "<%= config.sourcePath.js %>/**/*.js"
        tasks: ['uglify:dev']
      html:
        files: "build/craft/templates/**/*.html"
        tasks: []
  
  # Tasks
  grunt.registerTask 'default', ['watch']
  grunt.registerTask 'build', ['sass:dist', 'uglify:dist']