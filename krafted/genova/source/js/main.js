$(document).ready(function(){

	$('.js-menu_button').click(function() {
		if ($('.js-main_navigation').hasClass('nav_open')) {
			$('.js-main_navigation').removeClass('nav_open');
		} else {
			$('.js-main_navigation').addClass('nav_open');
		}
	});

	$('#recipeType').change(function(){
		var url = $(this).children(':selected').val();
		if (url) { 
			window.location = url;
		}
		return false;
	});
	
	$('.js-anchor').click(function(e) {
		e.preventDefault();
		var anchor = $(this).attr('href').split("#").pop();
		if ($('#'+anchor).length > 0) {
			var target = $('#'+anchor).offset().top;
			var scrollPoint = target+5;
			$('html, body').animate({
				scrollTop: [scrollPoint+"px", "easeInOutExpo"]
			}, 1000);
		} else {
			window.location = $(this).attr('href');
		}
	});
	
	$("form").validate({
		errorPlacement: function(error,element) {
			switch(element[0].type) {
				case "radio":
					error.appendTo(element.parents("fieldset"));
					break;
				case "select-one":
					error.insertAfter(element.parent())
					break;
				default:
					error.appendTo(element.parent());
					break;
			}
		}
	});
	
	// Pin It Fix
	$("img").each(function() {
		if(!$(this).hasClass("pinit")) {
			$(this).attr("nopin", true);
		}
	});
	
	// Parallax
	$(window).on("resize.parallax", function() {
		$("[data-parallax]").parallax("50%", 0.25);
	}).trigger("resize");
	
	// Products Drop Down
	(function() {
		var timeout, navItem = $(".nav-link-2"), menu = $(".products-drop-down");
		var clearMenu = function() {
			timeout = setTimeout(function() {
				menu.removeClass("slideInDown").addClass("slideOutUp");
				menu.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
					menu.removeClass("slideOutUp");
				});
			},300);
		};
		$(".nav-link-2").hoverIntent(function() {
			clearTimeout(timeout);
			menu.removeClass("slideOutUp").addClass("slideInDown");
		},function() {
			clearMenu();
		});
		menu.mouseover(function() {
			clearTimeout(timeout);
		});
		menu.mouseleave(function() {
			clearMenu();
		});
	})();
	
	// Nutrition Facts
	(function() {
		var container = $(".js-nutrition-facts");
		var isOpen = false;
		$(".js-nutrition-open,.js-nutrition-close").on("click", function(e){
			e.preventDefault();
			if(isOpen) {
				container.slideUp(500,'easeInOutExpo');
			}else{
				container.slideDown(500,'easeInOutExpo');
				$("html,body").animate({
					scrollTop: $(".nutrition_info").offset().top
				},500,'easeInOutExpo');
			}
			isOpen = !isOpen;
		});
	})();
	
});