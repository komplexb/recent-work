module.exports = (bower) ->
  [
    "#{bower}/jquery.easing/js/jquery.easing.js"
    "#{bower}/angular/angular.js"
    "#{bower}/angular-animate/angular-animate.js"
    "#{bower}/lodash/lodash.js"
    "#{bower}/jquery.customSelect/jquery.customSelect.js"
    "#{bower}/jquery-cycle2/build/jquery.cycle2.js"
    "#{bower}/jquery.payment/lib/jquery.payment.js"
    "#{bower}/spin.js/spin.js"
    "#{bower}/spin.js/jquery.spin.js"
    "#{bower}/video.js/dist/video-js/video.js"
    "main.coffee"
    "controllers/**/*"
    "services/**/*"
    "directives/**/*"
  ]
