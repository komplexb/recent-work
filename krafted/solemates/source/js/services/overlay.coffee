angular.module 'solemates'
  
  .factory 'Overlay', ($rootScope, $compile) ->
    
    activeOverlay: false
    overlays: []
    
    init: ($scope) ->
      @$scope = $scope
      @$overlayElement = $compile("<overlay-wrap ng-show='overlay.activeOverlay'></overlay-wrap")($scope)
      $("body").append @$overlayElement
    
    add: (id, element, group) ->
      @overlays.push
        id: id
        element: element
        group: group
      @$overlayElement.append element
    
    open: (id) ->
      @activeOverlay = _.findWhere @overlays, id: id
      $(document).on "keyup.overlay", (e) =>
        @close() if e.which == 27
        @$scope.$apply()
      @$overlayElement.on "click.overlay", (e) =>
        @close() if e.target == e.currentTarget
        @$scope.$apply()
      return
    
    close: ->
      if @hasVideo()
        @activeOverlay.element.find("video")[0].pause()
      @activeOverlay = false
      $(document).off ".overlay"
      return
    
    getIndex: (offset) ->
      index = _.indexOf(@getGroup(), @activeOverlay)
      if offset == "prev"
        index = index-1
      if offset == "next"
        index = index+1
      index
    
    getPrev: ->
      collection = @getGroup()
      return _.last(collection) if @isFirst()
      collection[ @getIndex("prev") ]
    
    getNext: ->
      collection = @getGroup()
      return _.first(collection) if @isLast()
      collection[ @getIndex("next") ]
    
    getGroup: (group = @activeOverlay.group) ->
      _.where @overlays, group: group
    
    isFirst: ->
      @activeOverlay.id == _.first(@getGroup()).id
    
    isLast: ->
      @activeOverlay.id == _.last(@getGroup()).id
    
    hasGroup: ->
      @activeOverlay.group?
    
    hasVideo: ->
      !!@activeOverlay.element.find("video").length
      
