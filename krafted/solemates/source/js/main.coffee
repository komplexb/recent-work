angular.module 'solemates', ['ngAnimate']

  .run ($rootScope) ->
    
    # Vendor
    # $(document).foundation()
    $("select").customSelect()
    
    # Global listener for 
    $rootScope.$on "navSlideComplete", ->
      $rootScope.navWasOpen = false
      $rootScope.$apply()
    
    # Toggle Nav
    $rootScope.toggleNav = ->
      $rootScope.navIsOpen = !$rootScope.navIsOpen
      $rootScope.navWasOpen = true
  
  .controller 'MainCtrl', ($rootScope, $scope, $timeout, Overlay) ->
    
    # Cart
    newProductTimer = null
    $scope.cart = window.cart
    $scope.newProductInCart = false
    $scope.updateNewProductInCart = (model) ->
      $timeout.cancel newProductTimer
      $scope.newProductInCart = model
      newProductTimer = $timeout ->
        $scope.newProductInCart = false
      , 5000
    $scope.updateCartModel = (cart) ->
      $scope.cart = cart
    
    # Overlays
    Overlay.init $scope
    $scope.overlay = Overlay