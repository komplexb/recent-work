angular.module 'solemates'
  
  .directive "aHref", ->
    restrict: "A"
    link: (scope, element, attrs) ->
      target = $("[anchor='#{attrs.aHref}']")
      $(element).on "click", (e) ->
        e.preventDefault()
        $("html,body").animate
          scrollTop: target.offset().top
        , 500, "easeInOutExpo"