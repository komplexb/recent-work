angular.module 'solemates'
  
  .directive "creditCard", ->
    restrict: "A"
    link: (scope, element, attrs, api) ->
      # Create Icon Element
      typeField = $("<input type='hidden' name='payment[card_type]'>")
      element.before typeField
      element.after "<i class='cardtype'></i>"
      element.payment "formatCardNumber"
      element.on "change", ->
        typeField.val $.payment.cardType(@value)