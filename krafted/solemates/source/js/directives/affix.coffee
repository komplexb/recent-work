angular.module 'solemates'
  
  .directive "affix", ->
    restrict: "A"
    link: (scope, element, attrs, api) ->
      # Create a clone
      ghost = element.clone()
      ghost
        .insertAfter element
        .css
          display: "none"
          visibility: "hidden"
        .removeAttr "affix"
      
      # Scroll / Resize Listener
      $(window).on "scroll.affix resize.affix", ->
        scrollTop = $(window).scrollTop()
        threshold = (if scope.active then ghost.offset().top else element.offset().top) - parseInt(element.css("margin-top"))
        if scrollTop >= threshold
          scope.active = true
          ghost.show()
          element.width ghost.width()
        else
          scope.active = false
          ghost.hide()
          element.removeAttr "style"
        scope.$apply()