angular.module 'solemates'

  .directive "checkoutForm", ($http, $compile, $timeout) ->
    restrict: "A"
    scope: true
    controller: ($scope, $element) ->
      
      $scope.showPromoCode = ->
        $scope.promoCode = true
        $timeout ->
          $("#promo_code").focus()
      
      @loadStep3 = ->
        $http.get "/checkout/step3"
          .success (data) ->
            $("[checkout-step='3']").html $compile(data)($scope)
            $("[checkout-step='3']").find("select").customSelect()
      
      @loading = (element,kill=false) ->
        return element.css("opacity","1").spin(false).find(":input").prop("disabled", false) if kill
        element.css
          position: if !element.hasClass("fixed") then "relative" else ""
          opacity: ".4"
        .spin $("[checkout-form-loader]").attr("checkout-form-loader"), $("[checkout-form-loader]").attr("spin-color")
        .find(":input").prop("disabled", true)
      
      @updateCart = (loader, callback, validate=true) ->
        loadingView = $("[checkout-form-loader]")
        loadingView = if loadingView.length then loadingView else $element
        data = $element.serializeArray()
        if $element.find("[name='submit']").length
          data.push
            name: "submit"
            value: true
        @loading(loadingView) if loader
        $.post "/", data, (response) =>
          $scope.$parent.updateCartModel response.cart if _.isObject response
          @loading loadingView, true if loader
          if response.cart.order_id?
            window.location = "/checkout/success/#{response.cart.order_hash}"
          if response.form_errors? and validate
            if response.form_errors.payment
              $scope.paymentError = true
            $("label.error").remove()
            _.forEach response.form_errors, (errorMsg, name) ->
              error = $("<label for='##{name}' class='error'>#{errorMsg}</label>")
              error.insertAfter $("##{name}").parent()
          else
            callback? response
          $scope.$apply()
        .error ->
          alert "Something went wrong! Please check your fields & try again or contact us at info@thesolemates.com"
      
      @addProduct = ->
        modifiers = do ->
          modifiers = []
          $("[product-modifier]").each ->
            modifiers.push $(@).find(":selected").text()
          modifiers
        $scope.$parent.updateNewProductInCart
          title: $("[product-title]").text()
          price: $("[product-price]").text()
          modifiers: modifiers
          image: $("[product-image]").attr("src")
    
    link: (scope, element, attrs, api) ->
      
      # if attrs.allowSubmit
      #   element.triggerHandler("$destroy")
      # else
      element.on "submit", ->
        api.updateCart true, (response) ->
          if attrs.checkoutForm == "product"
            $("html,body").animate
              scrollTop: 0
            , 500, "easeInOutExpo"
            api.addProduct()
          if attrs.nextStep > 2
            api.loadStep3()
          scope.gotoStep attrs.nextStep if attrs.nextStep
          scope.$apply()
      
      element.find("#shipping_same_as_billing").on "change", ->
        slider = if this.checked then "slideUp" else "slideDown"
        $("#shipping-fields")[slider] 500, "easeInOutExpo", ->
          $(@).find("input[type=text]:first:enabled").focus()
      .change()
      
      element.on "change", "[name='shipping_method']", ->
        summary = $(".checkout__summary")
        api.loading summary
        api.updateCart false, ->
          api.loading summary, true
        , false
  
  .directive "checkoutStep", ->
    restrict: "A"
    link: (scope, element, attrs) ->
      step = +attrs.checkoutStep
      element.hide() if step != 1
      scope.$watch "step", (newStep, prevStep) ->
        slider = if step == newStep then "slideDown" else "slideUp"
        element[slider] 500, "easeInOutExpo", ->
          if slider == "slideDown" and step > 1
            element.find("select").on "change", ->
              $(@).parent().find(".customSelectInner").html $(@).find(":selected").html()
            element.find("input[type=text]:first:enabled").focus()
  
  .directive "cartRow", ->
    require: "^checkoutForm"
    restrict: "A"
    link: (scope, element, attrs, api) ->
      element.find("[remove-from-cart]").on "click", (e) ->
        element.fadeOut -> element.remove()
        api.updateCart false
  
  .directive "itemQty", ->
    restrict: "A"
    link: (scope, element) ->
      
      increase = ->
        element.val (+element.val() + 1)
      
      decrease = ->
        element.val (+element.val() - 1) unless element.val() <= 1
      
      element.on "keydown", (e) ->
        if e.keyCode == 38
          increase()
        if e.keyCode == 40
          decrease()
      
      element.on "input", (e) ->
        @value = @value.replace(/[^0-9]/g, '');
      
      element.on "change", (e) ->
        @value = 0 if !@value
      
      element.on "click", (e) ->
        $(@).select()
      
      $("[item-qty-increase]").on "click", (e) ->
        e.preventDefault()
        increase()
      
      $("[item-qty-decrease]").on "click", (e) ->
        e.preventDefault()
        decrease()
  
  .directive "productSkuImage", ($timeout, $interval) ->
    restrict: "A"
    controller: ($scope, $element) ->
      ExpressoLoaded = false
      $scope.productSku = null
      @setSku = ->
        return unless ExpressoLoaded
        formData = window.ExpressoStore.serializeForm $("[checkout-form]")
        productData = window.ExpressoStore.matchSku formData
        $scope.productSku = productData.sku
        
      waitForExpresso = $interval =>
        if typeof window.ExpressoStore.serializeForm == "function" and typeof window.ExpressoStore.matchSku == "function"
          ExpressoLoaded = true
          @setSku()
          $interval.cancel waitForExpresso
      , 500
    link: (scope, element, attrs, api) ->
      $timeout ->
        $("[product-modifier]").on "change", ->
          api.setSku()
          scope.$apply()
    