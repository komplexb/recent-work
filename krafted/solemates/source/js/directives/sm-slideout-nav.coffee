angular.module 'solemates'
  
  .directive "smNavSlideout", ->
    restrict: "A"
    link: (scope, element, attrs) ->
      $(element).on "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", ->
        scope.$emit "navSlideComplete"