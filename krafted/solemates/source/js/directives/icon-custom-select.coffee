angular.module 'solemates'
  
  .directive "iconCustomSelect", ->
    restrict: "A"
    link: (scope, element, attrs, api) ->
      
      select = element.find "select"
      icon = $('<span class="item-attribute"></span>')
      type = attrs.iconCustomSelect.toLowerCase()
      
      select.on "change", ->
        element.find(".customSelectInner").prepend icon
        element.attr "data-item-#{type}", $(@).find(":selected").text().toLowerCase()
      .change()
      