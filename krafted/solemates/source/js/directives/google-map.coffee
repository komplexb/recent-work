angular.module 'solemates'

  .directive "googleMap", ->
    restrict: "E"
    transclude: true
    controller: ($scope, $transclude) ->
      $transclude (clone) ->
        $scope.infoContent = $("<div></div>").append($(clone)).html()
    link: (scope, element, attrs) ->
      
      window.googleMapHandler = ->
        
        marker = new google.maps.Marker
          position: new google.maps.LatLng attrs.latitude, attrs.longitude
        
        infowindow = new google.maps.InfoWindow
          content: scope.infoContent
        
        opts = 
          center: marker.getPosition()
          zoom: 18
          mapTypeId: google.maps.MapTypeId.ROADMAP
          disableDefaultUI: true
          scrollwheel: false
        
        map = new google.maps.Map element[0], opts
        
        # Apply Marker
        marker.setMap map
        
        # Center map on browser resize
        google.maps.event.addDomListener window, 'resize', ->
          center = map.getCenter()
          google.maps.event.trigger map, 'resize'
          map.setCenter center
          
        # Info Window
        google.maps.event.addListener marker, 'click', ->
          infowindow.open map, marker
      
      $.getScript "//maps.googleapis.com/maps/api/js?callback=googleMapHandler"