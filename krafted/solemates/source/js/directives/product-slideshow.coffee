angular.module 'solemates'
  
  .directive "productSlideshow", ->
    restrict: "E"
    controller: ($scope, $element) ->
      $scope.selectImage = (e) ->
        newThumb = $(e.currentTarget)
        $element.find(".active").removeClass "active"
        newThumb.addClass "active"
        $scope.activeImageSrc = newThumb.find("img").attr("src")
    link: (scope, element, attrs, api) ->
      