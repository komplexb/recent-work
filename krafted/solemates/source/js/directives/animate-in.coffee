angular.module 'solemates'
  
  .config ->
    # If we're rotating, let's reset animations
    $(".cycle-slideshow").on "cycle-before", (event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) ->
      animatedEls = $(incomingSlideEl).find "[animate-in]"
      animatedEls.removeClass "animate-in"
      $(incomingSlideEl).data "animatedEls", animatedEls
      
      $(outgoingSlideEl).find("[animate-in]").addClass "animate-out"
      
    $(".cycle-slideshow").on "cycle-after", (event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) ->
      $(incomingSlideEl).data("animatedEls")
        .removeClass "animate-out"
        .addClass "animate-in"
  
  .directive "animateIn", ($interval) ->
    restrict: "A"
    scope: true
    link: (scope, element, attrs) ->
      offset = +attrs.animateIn or 0
      wfCheck = $interval ->
        if $("html").hasClass "wf-active"
          $interval.cancel wfCheck
          $(window).on "scroll.animateIn", ->
            if ( element.offset().top - offset ) < ( $(window).scrollTop() + $(window).height() )
              element.addClass "animate-in"
          .scroll()
      , 100