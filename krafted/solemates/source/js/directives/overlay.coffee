angular.module 'solemates'
  
  .directive "overlayWrap", ->
    restrict: "E"
    templateUrl: "overlay.html"
  
  .directive "overlay", ($timeout, Overlay) ->
    restrict: "E"
    link: (scope, element, attrs) ->
      $timeout ->
        Overlay.add attrs.id, element, attrs.group
      element.on "submit", "form", ->
        Overlay.close()
        scope.$apply()
      scope.$watch attrs.ngShow, (shown) ->
        if shown
          $timeout ->
            element.find(":input:enabled:first").focus()
            element.find("video")[0].play() if Overlay.hasVideo()
