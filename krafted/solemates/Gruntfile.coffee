module.exports = (grunt) ->
  
  # Load grunt tasks automatically
  require("load-grunt-tasks")(grunt)
  
  grunt.initConfig
  
    pkg: grunt.file.readJSON('package.json')
    
    fontello:
      dist:
        options:
          config: "fontello.json"
          fonts: "build/public/assets/fonts"
          styles: "source/css/fontello"
          scss: true
          force: true