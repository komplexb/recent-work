paths =
  css:
    source: "source/css"
    build: "build/public/assets/css"
    import: [
      "./bower_components/bourbon/app/assets/stylesheets"
      "./bower_components/neat/app/assets/stylesheets"
      "./bower_components/foundation/scss"
    ]
  js:
    source: "source/js"
    build: "build/public/assets/js"
  img:
    source: "source/img"
    build: "build/public/assets/img"
  sprites:
    source: "source/sprites"
    build: "build/craft/templates/_partials"

gulp = require 'gulp'
plugins = require('gulp-load-plugins')()
svgSprite = require 'gulp-svg-sprite'
Notification = require 'node-notifier'
errorHandler = (e) ->
  notifier = new Notification();
  notifier.notify
    title: "Gulp Error"
    message: e.message
  @emit "end"
pngcrush = require 'imagemin-pngcrush'
devMode = false

########################################
# Stylesheets
########################################

gulp.task "css", ->
  gulp.src ["#{paths.css.source}/*.scss","!#{paths.css.source}/_*"]
    .pipe plugins.include()
    .pipe plugins.sass
      includePaths: paths.css.import
    .on "error", errorHandler
    .pipe plugins.if !devMode, plugins.cssmin()
    .pipe plugins.notify
      title: "SASS"
      message: "Compile Success!"
    .pipe gulp.dest paths.css.build

########################################
# Javascripts
########################################

gulp.task "js", ->
  gulp.src ["#{paths.js.source}/application.coffee"]
    .pipe plugins.include()
    .pipe plugins.coffee()
    .on "error", errorHandler
    .pipe plugins.notify
      title: "CoffeeScript"
      message: "Compile Success!"
    .pipe gulp.dest paths.js.build
    
  gulp.src ["#{paths.js.source}/vendor.js"]
    .pipe plugins.include()
    .pipe gulp.dest paths.js.build

########################################
# Images
########################################

gulp.task "imagemin", ->
  gulp.src "#{paths.img.source}/**/*"
    .pipe plugins.newer paths.img.build
    .pipe plugins.imagemin {progressive: true, use: [pngcrush()]}
    .on "error", errorHandler
    .pipe gulp.dest paths.img.build

########################################
# Sprites
########################################

gulp.task "sprites", ->
  gulp.src "#{paths.sprites.source}/*.svg"
    .pipe svgSprite
      mode:
        symbol:
          dest: ""
          sprite: "_sprite.html"
          inline: true
          example: false
    .pipe plugins.extReplace ".html"
    .pipe gulp.dest paths.sprites.build

########################################
# Watch (default task)
########################################

gulp.task 'default', ->
  devMode = true
  plugins.livereload.listen()
  gulp.watch(['build/public/assets/**/*']).on('change', plugins.livereload.changed);
  
  gulp.start(['css','js','sprites','imagemin'])
  gulp.watch "#{paths.css.source}/**/*", ['css']
  gulp.watch "#{paths.js.source}/**/*", ['js']
  gulp.watch "#{paths.img.source}/**/*", ['imagemin']
  gulp.watch "#{paths.sprites.source}/**/*.svg", ['sprites']
  

########################################
# Build
########################################

gulp.task 'build', ['css','js','sprites','imagemin']