angular.module 'futureadvisor'

  .directive "faHeader", ->
    restrict: "A"
    controller: ($scope, $transclude) ->
      # Opts
      @threshold = 150
      
      # Scope
      $scope.headerActive = false
      
      # API
      @toggle = ->
        if $(window).scrollTop() == 0 and $scope.headerActive
          $scope.headerActive = false
        if $(window).scrollTop() >= @threshold and !$scope.headerActive
          $scope.headerActive = true
        $scope.$apply()
    
    link: (scope, element, attrs, api) ->
      _.delay ->
        $(window).on "scroll", ->
          api.toggle()
        .trigger "scroll"