angular.module 'futureadvisor'
  
  .directive "dashboardShowcase", ->
    restrict: "E"
    controller: ($scope, $element) ->
      $scope.activeSlide = 0
      @slides = $element.find("ds-screens")
      @notes = $element.find("ds-notes")
      @animEvents = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend" # I am going to make this a jq plugin...
      @gotoSlide = (slide) =>
        @notes.children(":eq(#{$scope.activeSlide})").addClass("active transition-out").one @animEvents, (e) =>
          $(e.currentTarget).removeClass("transition-out active")
          @slides.cycle 'goto', slide
          $scope.activeSlide = slide
          @notes.children(":eq(#{slide})").addClass("transition-in").one "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", (e) ->
            $(@).addClass("active").removeClass("transition-in")
          $scope.$apply()
        
    link: (scope, element, attrs, api) ->
      _.delay ->
        api.slides.cycle
         slides: "> ds-screen"
         fx: "scrollHorz"
         easing: "easeInOutExpo"
         timeout: 0
         log: false
        
        # Init
        api.notes.children(":eq(#{scope.activeSlide})").addClass("transition-in").one api.animEvents, (e) ->
          $(@).addClass("active").removeClass("transition-in")
  
  .directive "slideHref", ->
    require: "^dashboardShowcase"
    restrict: "A"
    link: (scope, element, attrs, api) ->
      element.on "click", (e) ->
        e.preventDefault()
        api.gotoSlide $(@).attr("slide-href")
        $(@).addClass("active").siblings().removeClass("active")