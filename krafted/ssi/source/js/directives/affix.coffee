angular.module 'futureadvisor'
  
  .directive "affix", ->
    restrict: "A"
    link: (scope, element, attrs, api) ->
      # Create a clone
      ghost = element.clone()
      ghost
        .insertAfter element
        .css
          display: "none"
          visibility: "hidden"
        .removeAttr "affix"
      
      # Scroll / Resize Listener
      $(window).on "scroll.affix resize.affix", ->
        
        scrollTop = $(window).scrollTop()
        section = element.parents("section")
        topThreshold = (if scope.active then (ghost.offset().top-80) else (element.offset().top-80)) - parseInt(element.css("margin-top"))
        bottomThreshold = (scrollTop + element.height()) <= (section.offset().top + section.height())
        
        if scrollTop >= topThreshold and bottomThreshold
          scope.active = true
          ghost.show()
          element.width ghost.width()
        else
          scope.active = false
          ghost.hide()
          element.removeAttr "style"
        scope.$apply()