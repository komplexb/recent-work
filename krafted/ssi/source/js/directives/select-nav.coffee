angular.module 'futureadvisor'
  
  .directive "selectNav", ->
    restrict: "A"
    link: (scope, element, attrs, api) ->
      
      element.on "change", ->
        window.location = element.val()