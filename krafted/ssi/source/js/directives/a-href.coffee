angular.module 'futureadvisor'
  
  .directive "aHref", ->
    restrict: "A"
    controller: ($scope, $element) ->
    link: (scope, element, attrs, api) ->
      
      target = $("[anchor='#{attrs.aHref}']")
      
      # Are we in view?
      inView = ->
        $elem = target
        $window = $(window)
        docViewTop = $window.scrollTop()
        docViewBottom = docViewTop + $window.height()
        elemTop = $elem.offset().top
        elemBottom = elemTop + $elem.height()
        elemBottom <= docViewBottom and elemTop >= docViewTop
      
      $(element).on "click", (e) ->
        e.preventDefault()
        $("html,body").animate
          scrollTop: target.offset().top - $("#header").height() - parseInt(target.css("margin-top"))
        , 500, "easeInOutExpo"
      
      $(window).on "scroll.aHref", ->
        # console.log element.text(), element.offset().top,$(window).scrollTop()
        if (target.offset().top-250) <= $(window).scrollTop()
          element.parents(".sidebar-nav--onpage").find(".active").removeClass "active"
          element.parent().addClass "active"