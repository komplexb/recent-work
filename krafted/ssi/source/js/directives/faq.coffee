angular.module 'futureadvisor'
  
  .directive "faq", ->
    restrict: "A"
    scope: true
    link: (scope, element, attrs, api) ->
      scope.active = false
      toggle = element.find("[faq-toggle]")
      pane = element.find("[faq-pane]")
      pane.hide()
      toggle.on "click", ->
        if scope.active
          pane.slideUp "fast", "easeInOutExpo"
          scope.active = false
        else
          pane.slideDown "fast", "easeInOutExpo"
          scope.active = true
      